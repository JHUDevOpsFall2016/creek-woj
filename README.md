This is for JHU EN.605.409 (DevOps) Fall semester 2016. The code is taken from 
[https://github.com/martin-chris/creek-woj](https://github.com/martin-chris/creek-woj).

The notes on how this project was created and populated can be found on the [wiki](https://gitlab.com/JHUDevOpsFall2016/creek-woj/wikis/home).